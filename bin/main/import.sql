insert into user (name, username, email, password, role) values ('Alberto Martínez Ballesteros', 'Alberto', 'alberto.martinez@mimacom.com', '$2a$10$fTZpwguB0/SyqQU8tyFz0ODb2d30CEttMIL7KoNYSXXV5xlr5ROya', 'ROLE_USER, ROLE_ADMIN');
insert into user (name, username, email, password, role) values ('Pedro Picapiedra', 'Pedro', 'pedro.picapiedra@mimacom.com', '$2a$10$Tul5B95GQld1OFRV3MJuCOtXkA5lSHvC/U61QblnLk1Xt3Z7TseYW', 'ROLE_USER');

insert into task (name, description, author, complete) values ('Listar todas las tareas', 'Tarea 1', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Añadir seguridad básica con base de datos de usuarios en memoria', 'Tarea 2', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Añadir páginas de acceso público y privado', 'Tarea 3', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Diferenciar entre roles: USER y ADMIN', 'Tarea 4', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Distintos tipos de autenticación', 'Tarea 5', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Añadir página personalizada de login', 'Tarea 6', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Añadir página personalizada de logout', 'Tarea 7', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Mostrar datos de contexto del usuario', 'Tarea 8', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Los usuarios normales sólo pueden ver sus tareas', 'Tarea 9', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Esta tarea no debe aparecer más en la lista', 'Tarea 10', 'Pedro', 0);
insert into task (name, description, author, complete) values ('Seguridad a nivel de método', 'Tarea 11', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Leer la asignación de usuarios de base de datos', 'Tarea 12', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Encriptación de contraseñas', 'Tarea 13', 'Alberto', 0);
insert into task (name, description, author, complete) values ('Registro de usuarios con auto login', 'Tarea 14', 'Alberto', 0);

