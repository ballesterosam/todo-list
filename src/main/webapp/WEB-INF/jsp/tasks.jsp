<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<title>ToDo List</title>
</head>
<body>
	<h1>ToDo List</h1>
	
	<form class="form-inline" method="post" action="/addTask" >
	  <label for="name">Tarea</label>
	  <input type="text" class="form-control" id="name" name="name" placeholder="Tarea">
	
	  <label for="author">Autor</label>
	  <div class="input-group">
	    <div class="input-group-addon">@</div>
	    <input type="text" class="form-control" id="author" name="author" placeholder="Usuario">
	  </div>
	
	  <button type="submit" class="btn btn-primary">Submit</button>
	</form>
	
	<br />
	
	<c:if test="${not empty tasks}">
		<ul class="list-group mt-5">
			<c:forEach items="${tasks}" var="task">
				<li class="list-group-item">
					<a href="/task/${task.id}">${task.name}</a>
				</li>
			</c:forEach>
		</ul>
	</c:if>
</body>
</html>