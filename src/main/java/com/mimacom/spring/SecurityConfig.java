package com.mimacom.spring;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
	
    protected void configure(HttpSecurity http) throws Exception {
		http
			//.httpBasic()
			.formLogin()
    			.loginPage("/").permitAll()
    			.defaultSuccessUrl("/tasks")
    		.and()
    			.logout()
    			.logoutUrl("/logout")
    			.logoutSuccessUrl("/")
			.and()
				.authorizeRequests()
					.antMatchers("/search", "/todo", "/tasks", "/task/*", "/api/**").hasRole("USER")
					.antMatchers("/h2-console/**", "/admin").hasRole("ADMIN")
					.antMatchers("/", "/css/**").permitAll()
			.and()
				.headers().frameOptions().disable()
			.and()
				.csrf().disable();//.ignoringAntMatchers("/h2-console/**");
	}
	
	
	/*protected void configure(HttpSecurity http) throws Exception {
		http
		  .sessionManagement()
		  .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
		  .httpBasic()
		  .realmName("Login")
		.and()
			.authorizeRequests().antMatchers("/api/**").authenticated()
		.and()
		  .csrf().disable();

	}*/
	
	@Autowired
	/*
    public void configureGlobal(DataSource dataSource, AuthenticationManagerBuilder auth) throws Exception {
        auth
        .jdbcAuthentication().passwordEncoder(new BCryptPasswordEncoder())
			.dataSource(dataSource)
			.usersByUsernameQuery("select username, password, true from user where username = ?")
			.authoritiesByUsernameQuery("select username, role from user where username = ?")
		.and()
        	.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
	                .withUser("user").password("$2a$10$JmxNl1QUOoUY982h9yegguzFX9SIZtaEGej9XQIRZYfvItCR0UkmS").roles("USER")
	              .and()
	                .withUser("admin").password("$2a$10$rVFaXlQ8CwrzbLHLYg/5n.sCBF4qygOkMHpMpw1IMAK5AjeHCEG1O").roles("USER", "ADMIN");
	}*/
	
	
	public void configureGlobal (UserDetailsService userDetailsService, AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService)
			.passwordEncoder(new BCryptPasswordEncoder());
	}
	
	
	/*
	@Bean
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}


	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey("1234567890asdfg");
		return converter;
	}

	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}

	@Bean
	public DefaultTokenServices tokenServices() {
		DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(tokenStore());
		defaultTokenServices.setSupportRefreshToken(true);
		return defaultTokenServices;
	}
	*/
}