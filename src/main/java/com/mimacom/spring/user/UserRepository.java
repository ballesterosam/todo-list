package com.mimacom.spring.user;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface UserRepository extends JpaRepository<User, Long>{
	User findByUsername (String username);
	
	@Query(nativeQuery=false)
	User findByEmailCustom (@Param("email") String email);
}
