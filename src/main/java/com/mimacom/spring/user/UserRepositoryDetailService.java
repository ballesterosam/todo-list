package com.mimacom.spring.user;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserRepositoryDetailService implements UserDetailsService {
	private final UserRepository userRepository;
	
	@Autowired
	public UserRepositoryDetailService(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final User user = userRepository.findByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException("No se ha encontrado el usuario: " + username);
		}
		
		return new CustomUserDetails(user);
	}
	
	private static final class CustomUserDetails extends User implements UserDetails {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -3649586460767817040L;

		private CustomUserDetails (User user) {
			this.setEmail(user.getEmail());
			this.setName(user.getName());
			this.setId(user.getId());
			this.setPassword(user.getPassword());
			this.setRole(user.getRole());
			this.setUsername(user.getUsername());
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return AuthorityUtils.commaSeparatedStringToAuthorityList(getRole());
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}
		
	}

}
