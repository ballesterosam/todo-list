package com.mimacom.spring.task.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.NOT_FOUND)
public class TaskNotFoundException extends RuntimeException {

	public TaskNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TaskNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public TaskNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TaskNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TaskNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
