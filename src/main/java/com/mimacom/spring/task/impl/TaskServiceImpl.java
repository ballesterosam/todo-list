package com.mimacom.spring.task.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mimacom.spring.task.Task;
import com.mimacom.spring.task.TaskRepository;
import com.mimacom.spring.task.TaskService;
import com.mimacom.spring.user.User;
import com.mimacom.spring.user.UserRepository;

@Service
public class TaskServiceImpl implements TaskService{
	private static final String ALL_TASKS_CACHE = "tasks";
	
	private TaskRepository taskRepository;
	private UserRepository userRepository;

	@Autowired
	public TaskServiceImpl(TaskRepository taskRepository, UserRepository userRepository) {
		this.taskRepository = taskRepository;
		this.userRepository = userRepository;
	}

	@Override
	//@Cacheable (value={ALL_TASKS_CACHE})
	public List<Task> getTasks() {
		return taskRepository.findAll();
	}
	
	//@CacheEvict(value={ALL_TASKS_CACHE}, allEntries=true)
	@Transactional(readOnly=false)//, propagation=Propagation.NEVER)
	public Task addTask(Task task) {
		final Task newTask = taskRepository.save(task);
		
		return newTask;
	}
	
}
