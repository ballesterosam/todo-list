	package com.mimacom.spring.task;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PostAuthorize;

public interface TaskRepository extends JpaRepository<Task, Long>, JpaSpecificationExecutor<Task>{
	List<Task> findByDescriptionOrNameIgnoreCaseContaining (String description, String username);
	
	@Query("select t from Task t where author.username = :author")
	@RestResource(path="/by-author", rel="/by-author")
	List<Task> findByAuthor (@Param("author") String author);
	
	@Query("select t from Task t where (t.description like %:pattern% or t.name like %:pattern%) and author.username = :author")
	@RestResource(exported=false)
	List<Task> findByDescriptionOrNameIgnoreCaseContainingAndAuthorCustom (@Param("pattern") String pattern, @Param("author") String author);
	
	List<Task> findByDescriptionOrNameIgnoreCaseContainingAndAuthorUsername (String description, String Name, String username);
	
	@PostAuthorize("returnObject?.get().author.username == principal?.username")
	Optional<Task> findById (Long id);
	//Like
	//StartingWith
}
