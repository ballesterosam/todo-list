package com.mimacom.spring.task.api;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.mvc.BasicLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mimacom.spring.task.Task;
import com.mimacom.spring.task.TaskRepository;

//@Controller
@RestController
@RequestMapping("/api/task")
public class TaskApiController {
	
	private final TaskRepository taskRepository;
	private final ResourceAssembler<Task, TaskResource> resourceAssembler;

	@Autowired
	public TaskApiController(TaskRepository taskRepository, ResourceAssembler<Task, TaskResource> resourceAssembler) {
		this.taskRepository = taskRepository;
		this.resourceAssembler = resourceAssembler;
	}
	
	@GetMapping("/")
	//@ResponseBody
	public List<Task> getTasks() {
		return taskRepository.findByAuthor("Alberto");
	}
	
	@GetMapping(path="/hateoas", produces={MediaType.APPLICATION_JSON_VALUE})
	public PagedResources<TaskResource> getUserTasks(Pageable pageable, PagedResourcesAssembler<Task> assembler) {
		Page<Task> tasks = taskRepository.findAll(pageable);
		return assembler.toResource(tasks, resourceAssembler);
	}
	
	@GetMapping("/get")
	public Task getTaskById(@RequestParam(required=true) Long id) {
		return taskRepository.findById(id).get();
	}
	
	@GetMapping("/{itastasd}")
	public Task getTask(@PathVariable Long id) {
		return taskRepository.findById(id).get();
	}
	
	@GetMapping("/hateoas/{id}")
	public TaskResource getTaskAsResource (@PathVariable Long id) {
		final Task task = taskRepository.findById(id).get();
		final TaskResource taskResource = resourceAssembler.toResource(task);
		
		//taskResource.add(linkTo(methodOn(TaskApiController.class).getTasks()).withRel("all-tasks"));
		taskResource.add(BasicLinkBuilder.linkToCurrentMapping().slash("api/task/hateoas").withRel("all-tasks"));
		
		return taskResource;
	}
	
	@PostMapping("/save")
	public ResponseEntity<?> saveTask(@RequestBody @Valid Task task, Errors errors) {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(
				errors.getAllErrors().stream()
					.map(e->e.getCode())
					.collect(Collectors.toList())
			);		
		}
		
		return ResponseEntity.ok().body(taskRepository.save(task));
	}
}
