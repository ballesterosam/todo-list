package com.mimacom.spring.task.api;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.hibernate.loader.plan.build.internal.returns.SimpleEntityIdentifierDescriptionImpl;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import com.mimacom.spring.task.Task;

@Component
public class TaskResourceAssembler extends ResourceAssemblerSupport<Task, TaskResource> {

	public TaskResourceAssembler() {
		super(TaskApiController.class, TaskResource.class);
	}

	@Override
	public TaskResource toResource(Task task) {
		//TaskResource taskResource = new TaskResource(task);
		//taskResource.add(linkTo(methodOn(TaskApiController.class).getTaskAsResource(task.getId())).withSelfRel());
		TaskResource taskResource = createResourceWithId(task.getId(), task);
		return taskResource;
	}
	
	

}
