package com.mimacom.spring.task.api;

import org.springframework.hateoas.ResourceSupport;

import com.mimacom.spring.task.Task;

public class TaskResource extends ResourceSupport {
	private Task task;
	
	public TaskResource() {
	}

	public TaskResource(Task task) {
		this.task = task;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}
}
