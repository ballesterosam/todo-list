package com.mimacom.spring.task;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class TaskValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return Task.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Task task = (Task) target;
		
		System.out.println(task);
		
		validateName (task.getName(), errors);
		//ValidationUtils.rejectIfEmpty(errors, "author", "author.empty");
	}
	
	private void validateName(String name, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "name", "name.empty");
		
		if (name.length() < 5) {
			errors.rejectValue("name", "name.too.short");
		}
	}

}
