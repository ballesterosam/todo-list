package com.mimacom.spring.task;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.mimacom.spring.user.User;

public class TaskSpecification {
	public static Specification<Task> hasAuthor (User user) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("author"), user);
	}
	
	public static Specification<Task> contains (String text) {
		return Specification.where(nameContains(text)).or(descriptionContains(text));
	}
	
	public static Specification<Task> nameContains (String text) {
		return new Specification<Task>() {
			@Override
			public Predicate toPredicate(Root<Task> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return criteriaBuilder.like(root.get("name"), "%" + text + "%");
			}
		};
	}
	
	public static Specification<Task> descriptionContains (String text) {
		return new Specification<Task>() {
			@Override
			public Predicate toPredicate(Root<Task> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return criteriaBuilder.like(root.get("description"), "%" + text + "%");
			}
		};
	}
}
