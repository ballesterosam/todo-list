package com.mimacom.spring.task;

import java.util.List;

public interface TaskService {
	List<Task> getTasks();
	Task addTask(Task task);
}
