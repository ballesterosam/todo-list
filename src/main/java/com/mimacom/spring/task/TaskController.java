package com.mimacom.spring.task;

import static com.mimacom.spring.task.TaskSpecification.contains;
import static com.mimacom.spring.task.TaskSpecification.hasAuthor;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mimacom.spring.task.exception.TaskNotFoundException;
import com.mimacom.spring.user.User;
import com.mimacom.spring.user.UserRepository;

@Controller
public class TaskController {
	private final TaskRepository taskRepository;
	private final UserRepository userRepository;
	private final TaskService taskService;
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new TaskValidator());
    }
	
	@Autowired
	public TaskController(TaskRepository taskRepository, UserRepository userRepository, TaskService taskService) {
		this.taskRepository = taskRepository;
		this.userRepository = userRepository;
		this.taskService = taskService;
	}

	@GetMapping("/tasks")
	public String tasks(Model model) {
		//model.addAttribute("tasks", taskRepository.findAll());
		//model.addAttribute("task", new Task());
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println(user.getEmail());
		
		return "tasks";
	}

	@GetMapping("/task/{id}")
	public String detail(Model model, @PathVariable(name="id") Long id) {
		final Optional<Task> task = taskRepository.findById(id);
		
		if (task.isPresent()) {
			model.addAttribute("task", task.get());
			return "detail";
		} 
		
		throw new TaskNotFoundException("No existe la tarea con id: " + id);
	}
	
	@PostMapping("/addTask")
	public String addTask(@Valid @ModelAttribute ("task") Task task, BindingResult bindingResult, Model model) {
		
		if (!bindingResult.hasErrors()) {
			task.setDescription("Web task");
			task.setComplete(Boolean.FALSE);
			task.setAuthor(userRepository.findByUsername("Alberto"));
			
			//taskRepository.save(task);
			
			taskService.addTask(task);
			
			return "redirect:/tasks";
		}
		
		model.addAttribute("task", task);
		return "tasks";
	}
	
	@PostMapping("/search")
	public String search (@RequestParam("search-pattern") String searchPattern, Model model, Principal principal) {
		System.out.println(searchPattern);
		//final User user = userRepository.findByEmailCustom("alberto.martinez@mimacom.com");
		//List<Task> tasks = taskRepository.findByDescriptionOrNameIgnoreCaseContaining(searchPattern, searchPattern);
		//List<Task> tasks = taskRepository.findByDescriptionOrNameIgnoreCaseContainingAndAuthorCustom(searchPattern, user.getUsername());
		//List<Task> tasks = taskRepository.findByDescriptionOrNameIgnoreCaseContainingAndAuthorUsername(searchPattern, searchPattern, user.getUsername());
		
		final User user = userRepository.findByUsername(principal.getName());
		List<Task> tasks = taskRepository.findAll(hasAuthor(user).and(contains(searchPattern))); //Ctrl + Shift + M
		model.addAttribute("tasks", tasks);
		return "tasks";
	}
	
	@ModelAttribute("tasks")
	public List<Task> getTasks(Principal principal) {
		//Page<Task> tasks = taskRepository.findAll(PageRequest.of(0, 5));
		//return tasks.getContent();
		//return taskRepository.findAll();
		//return taskService.getTasks();
		System.out.println(principal.getName());
		return taskRepository.findByAuthor(principal.getName());
		//final User user = userRepository.findByUsername(principal.getName());
		//return taskRepository.findAll(TaskSpecification.hasAuthor(user));
	}
	
	@ModelAttribute("task")
	public Task createTask() {
		return new Task();
	}
	
	@ExceptionHandler(TaskNotFoundException.class)
	public String error404 (Model model, Exception e) {
		model.addAttribute("error", e.getMessage());
		return "error404";
	}
}

//TODO: https://tinyurl.com/mimacom-spring-trainning

// List tasks (V)--
// Internacionalización (V)--
// Add task (V)
// View detail (V)
// Validaciones (V) con test
// @ModelAttribute (V)
// Test de integración
// Generated Id (AUTO: global, IDENTITY: por entidad padre, SEQUENCE: secuencia, min=1, max, allocationSize, TABLE: initialValue=0, allocationSize) (V)
// Usuarios (V)
// Excepciones (V)
// Paginacion (V)
// Buscador exacto (V)
// Buscador personalizado (V)


// Specifications(V)
// Cache (V)
// Transacciones(V)

// Spring Data REST - Cambiar path, ocultar (V)
// Hal Browser (V)
// MessageBody (V)
// Spring REST Controller (V)
// HATEOAS (V)

// Security -dep (V)
// http - basic (V)
// users - in memory (V)
// users - database (V)
// login - form (V)
// pags. publicas y privadas - user y admin (V)
// pagina de login (V)
// logout (V)
// datos de contexto (V)
// filtro en la vista (V)
// los usuarios solo ven sus tareas (V)
// seguridad a nivel de método (V)
// usuarios del repositorio (V)
// SecurityContextHolder (V)
// oauth (V)

// Docker





// -- Base de datos MySQL


// -- @ManyToMany (tags)


// Borrar tasks
// Completar tasks

