package com.mimacom.spring;

import com.mimacom.spring.task.Task;
import com.mimacom.spring.user.User;

public class TestResources {
	public static Task SampleValidTask = new Task(1, "MyTask", "Description", false);
	public static Task SampleValidTask2 = new Task(2, "MyTask2", "Description2", false);
	
	public static Task SampleInvalidTask = new Task(1, "Task", "Description", false);
	
	public static User SampleUser = new User(1, "Alberto Martínez", "Alberto", "alberto.martinez@mimacom.com");
}
