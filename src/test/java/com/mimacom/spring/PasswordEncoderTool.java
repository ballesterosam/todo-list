package com.mimacom.spring;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordEncoderTool {

	public static void main(String[] args) {
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		System.out.println("User: " + passwordEncoder.encode("user"));
		System.out.println("Admin: " + passwordEncoder.encode("admin"));
		System.out.println("Pedro: " + passwordEncoder.encode("pedro"));
		System.out.println("Alberto: " + passwordEncoder.encode("alberto"));
	}

}
