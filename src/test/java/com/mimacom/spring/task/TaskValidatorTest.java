package com.mimacom.spring.task;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskValidatorTest {
	
	@Autowired
	@Qualifier("taskValidator")
	private Validator taskValidator;
	
	@Test
	public void supports() {
        assertThat(taskValidator.supports(Task.class)).isTrue();
        assertThat(taskValidator.supports(Object.class)).isFalse();
    }
	
	@Test
	public void validateValidTask() {
		final Task task = new Task (0, "mytask", "description", false);
		final Errors errors = new BeanPropertyBindingResult(task, "task");
		
		taskValidator.validate(task, errors);
		
		assertThat(errors.hasErrors()).isFalse();
	}
	
	@Test
	public void validateTaskWithNameTooShort() {
		final Task task = new Task (0, "name", "description", false);
		final Errors errors = new BeanPropertyBindingResult(task, "task");
		
		taskValidator.validate(task, errors);
		
		assertThat(errors.hasErrors()).isTrue();
		assertThat(errors.getFieldError("name").getCode()).isEqualTo("name.too.short");
	}
}
