package com.mimacom.spring.task;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mimacom.spring.TestResources;
import com.mimacom.spring.user.UserRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(TaskController.class)
//@AutoConfigureMockMvc
//@SpringBootTest
public class TaskControllerTest {
	@Autowired
    private MockMvc mockMvc;
	
	@MockBean
	private TaskRepository taskRepository;
	
	@MockBean
	private UserRepository userRepository;
	
	@Test
	public void testGetTasks() throws Exception{
		List<Task> tasks = new ArrayList<>();
		tasks.add (TestResources.SampleValidTask);
		tasks.add (TestResources.SampleValidTask2);
		
		when(taskRepository.findAll()).thenReturn(tasks);
		
		mockMvc.perform(get("/")).andExpect(status().isOk())
			.andExpect(model().attribute("tasks", hasSize(2)))
			.andExpect(view().name("tasks"));
		
		verify(taskRepository, times(1)).findAll();
	}
	
	@Test
	public void testGetDetail() throws Exception{
		Task task = TestResources.SampleValidTask;
		task.setAuthor(TestResources.SampleUser);
		
		when(taskRepository.findById(task.getId())).thenReturn(Optional.of(task));
		
		mockMvc.perform(get("/task/{id}", 1L)).andExpect(status().isOk())
			.andExpect(model().attribute("task", task))
			.andExpect(model().attribute("task", hasProperty("description", is(task.getDescription()))));
		
		verify(taskRepository, times(1)).findById(task.getId());
	}
	
	@Test
	public void testAddValidTask() throws Exception{
		Task task = new Task(0, "mytask", "Web task", false);
		
		when(taskRepository.save(task)).thenReturn(task);

		mockMvc.perform(post("/addTask")
				//.param("author", task.getAuthor().getName())
				.param("name", task.getName())
				.sessionAttr("task", task))
		.andExpect(status().is3xxRedirection());
		
		//assertThat(taskRepository.findAll().size()).isEqualTo(15);
		
		verify(taskRepository, times(1)).save(task);
	}

	@Test
	public void testAddInvalidTask() throws Exception{
		Task task = TestResources.SampleInvalidTask;
		
		when(taskRepository.save(task)).thenReturn(task);
		
		mockMvc.perform(post("/addTask")
				//.param("author", task.getAuthor().getName())
				.param("name", task.getName())
				.sessionAttr("task", task))
		.andExpect(status().isOk());
		
		verify(taskRepository, times(0)).save(task);
	}
}
